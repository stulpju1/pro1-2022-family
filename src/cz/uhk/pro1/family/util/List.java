package cz.uhk.pro1.family.util;

public interface List<E> extends Iterable<E> {

    public void add(E element);
    public void add(E element, int index);
    public void remove(int index);
    public E get(int index);
    public int size();

}
