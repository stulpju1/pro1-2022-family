package cz.uhk.pro1.family.services;

import cz.uhk.pro1.family.model.Family;
import cz.uhk.pro1.family.model.Person;

public class VitalRecordsOffice implements Office {

    @Override
    public String check(Family f) {
        String report = "";
        if (f.getMother() != null) {
            report += "matka: " + f.getMother() + ", "; // report += "matka: " + f.getMother().toString() + ", ";
        }
        if (f.getFather() != null) {
            report += "otec: " + f.getFather() + ", ";
        }
        if (f.getChildren().size() > 0) {
            report += "děti: ";
//            for (int i = 0; i < f.getChildren().size(); i++) {
//                report += f.getChildren().get(i) + ", ";
//            }
            for (Person child : f.getChildren()) {
                report += child + ", ";
            }
        }
        return report;
    }

}
