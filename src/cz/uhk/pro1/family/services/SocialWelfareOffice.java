package cz.uhk.pro1.family.services;

import cz.uhk.pro1.family.model.Family;

public class SocialWelfareOffice implements Office {

    @Override
    public String check(Family f) {
        if (f.getMother() == null || f.getFather() == null) {
            return "rodina je neúplná";
        } else {
            return "rodina je úplná";
        }
//        return (f.getMother() == null || f.getFather() == null) ? "rodina je neúplná" : "rodina je úplná";
    }

}
