package cz.uhk.pro1.family.app;

import cz.uhk.pro1.family.model.Person;
import cz.uhk.pro1.family.util.ArrayList;
import cz.uhk.pro1.family.util.LinkedList;
import cz.uhk.pro1.family.util.List;

public class ListApp {

    public static void main(String[] args) {

        List<Person> peopleArrayList = new ArrayList<>();
        peopleArrayList.add(new Person("Leonard", "Hofstadter"));
        peopleArrayList.add(new Person("Sheldon", "Cooper"));
        peopleArrayList.add(new Person("Howard", "Wolowitz"));
        peopleArrayList.add(new Person("Raj", "Koothrappali"));
        System.out.println(peopleArrayList.size());
        System.out.println(peopleArrayList.get(1));

        List<Person> peopleLinkedList = new LinkedList<>();
        peopleLinkedList.add(new Person("Penny", ""));
        peopleLinkedList.add(new Person("Amy", ""));
        peopleLinkedList.add(new Person("Bernadette", ""));
        System.out.println(peopleLinkedList.size());
        System.out.println(peopleLinkedList.get(1));

    }

}
